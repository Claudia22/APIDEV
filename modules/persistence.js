
'use strict'

const schema = require('../schema/schema')

exports.saveBook = bookDetails => new Promise( (resolve, reject) => {
	if (!'title' in bookDetails && !'authors' in bookDetails && !'description' in bookDetails) {
		reject(new Error('invalid book object'))
	}
	const book = new schema.Book(bookDetails)

	book.save( (err, book) => {
		if (err) {
			reject(new Error('an error saving book'))
		}
		resolve(book)
	})
})

exports.saveOrder = (user,book) => new Promise( (resolve, reject) => {

	let us
	let order
	let qt

	
	schema.User.find({firstname:user},(err,docs) =>{
		if(err){
			reject(new Error('user not found'))	
		}
		
		us = new schema.User(docs[0])
		
		if(us.status == 'logout') reject(new Error('user is logged in'))
	})
	
	schema.Book.find({title : book}, (err, docs) => {
		
		if (err){
			reject(  new Error('Book not available'))
		}
		
		const bk = new schema.Book(docs[0])
		 qt = bk.quantity 
		
		if(qt < 0) reject(new Error('book is out of stock'))
	
		bk.quantity = qt - 1
	// 	bk.save( (err, docs) => {
	// 		if (err) {
	// 			reject(new Error('an error saving book'))
	// 	}
	// })		
		
	})
	
	
	
	

	
	order.save( (err, docs) => {
		if (err) {
			reject(new Error('an error creating order'))
		}
		resolve(docs)
	})
})


exports.getAllBooks = () => new Promise( (resolve, reject) => {

	schema.Book.find({}, (err, docs) => {
		
		if (err){
		console.log('Error -- database error')
		}
			resolve(docs)
	})	
})

//update book quantity using find method of the schema
exports.updateBook = (bookDetails,quantity) => new Promise( (resolve, reject) => {

	if (!'title' in bookDetails && !'authors' in bookDetails && !'description' in bookDetails) {
		reject(new Error('invalid book object'))
	}
	
	
	schema.Book.find({title: bookDetails.title}, (err, docs) => {
		
	
		const book = new schema.Book(docs[0])	
		book.quantity = book.quantity + quantity
		


		book.save( (err, book) => {
		if (err) {
			reject(new Error('an error saving book'))
		}
			resolve(book)
		})
	})	
})

//Search book promise using find method of the schema
exports.bookSearch = book => new Promise( (resolve, reject) => {
	schema.Book.find({title: book}, (err, docs) => {
		if (err) reject(new Error('database error'))
		if (!docs.length) reject(new Error('book is  not avaliable'))	
		const book = new schema.Book(docs[0])
		resolve(book)
	})
})

exports.addAccount = details => new Promise( (resolve, reject) => {
	if (!'username' in details && !'password' in details && !'name' in details) {
		reject(new Error('invalid user object'))
	}
	const user = new schema.User(details)
	user.status = 'login'

	user.save( (err, user) => {
		if (err) {
			reject(new Error('error creating account'))
		}
		delete details.password
		resolve(details)
	})
})

exports.accountExists = account => new Promise( (resolve, reject) => {
	schema.User.find({username: account.username}, (err, docs) => {
		if(err) reject(new Error('error connecting to database'))
		if (docs.length) reject(new Error(`username already exists`))
		
		resolve()
	})

})

exports.getUser = account => new Promise( (resolve, reject) => {
	schema.User.find({username: account.username}, (err, docs) => {
		if(err) reject(new Error('error connecting to database'))
		if (docs.length) resolve(docs[0])
	})
})


exports.login = account => new Promise( (resolve, reject) => {
	schema.User.find({username: account.username}, (err, docs) => {
		let user
		if (err) reject(new Error('database error'))
		if (docs.length) {
			user = new schema.User(docs[0])
			user.status = 'login'
		}

		user.save( (err, user) => {
		if (err) {
			reject(new Error('error logging in'))
			} 
		
		})
		resolve(docs)
	})
})

exports.logout = account => new Promise( (resolve, reject) => {
	schema.User.find({username: account.username}, (err, docs) => {
		let user
		if (err) reject(new Error('database error'))
		if (docs.length) {
			user = new schema.User(docs[0])
			user.status = 'logout'
		}

		user.save( (err, user) => {
		if (err) {
			reject(new Error('error logging in'))
			}
		
		})
		resolve('logout')
	})
})



exports.getCredentials = credentials => new Promise( (resolve, reject) => {
	schema.User.find({username: credentials.username}, (err, docs) => {
		if (err) reject(new Error('database error'))
		if (docs.length) resolve(docs)
		reject(new Error(`invalid username`))
	})
})

exports.bookExists = (username, book) => new Promise( (resolve, reject) => {
	schema.Book.find({account: username, bookID: book}, (err, docs) => {
		if (err) reject(new Error('database error'))
		if (docs.length) reject(new Error('book already in cart'))
		resolve()
	})
})

exports.getBooksInCart = user => new Promise( (resolve, reject) => {
	schema.Book.find({account: user}, (err, docs) => {
		if (err) reject(new Error('database error'))
		if (!docs.length) reject(new Error('shopping cart empty'))
		resolve(docs)
	})
})