
'use strict'

// import the mongoose package
const mongoose = require('mongoose')

const username = process.env.MONGO_USER || 'claudia'
const password = process.env.MONGO_PASS || 'password123'
const host = process.env.MONGO_HOST || 'ds145302.mlab.com'
const port = process.env.MONGO_PORT || 45302
const database = process.env.MONGO_DB || 'bookshopdb'

mongoose.connect(`mongodb://${username}:${password}@${host}:${port}/${database}`)
mongoose.Promise = global.Promise
const Schema = mongoose.Schema

// create a schema
const userSchema = new Schema({
	name: String,
	username: String,
	password: String,
	status: String
})

// create a model using the schema
exports.User = mongoose.model('User', userSchema)

// create a schema
const bookSchema = new Schema({
  account: String,
	title: String,
	authors: String,
	description: String,
	quantity: {type:Number, default:0},
  bookID: String
})

// create a model using the schema
exports.Book = mongoose.model('Book', bookSchema)



// create a schema for the order
const orderSchema = new Schema({
  username: String,
	bookID: []
})

// create a model using the schema
exports.Order = mongoose.model('Order', bookSchema)


